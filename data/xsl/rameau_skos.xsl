<?xml version="1.0" encoding="UTF-8"?>
<!--
    Description : Convertit un fichier Rameau de data.bnf.fr en un thésaurus
    simplifié des descripteurs, génériques, spécifiques, associés, synonymes,
    avec les uri.

    Ce fichier est complet, mais les libellés des termes liés ne sont pas indiqués.

    - Uniquement data.bnf (pas les références aux autres thésaurus, par exemple
    http://id.loc.gov/authorities/subjects ou les événements).
    - Choix du label en français, sinon le premier.
    - Uniquement les descripteurs avec un ark (pas les "/vocabulary/rameau").

    @copyright Daniel Berthereau, 2019-2020
    @license CeCILL 2.1 https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bnf-onto="http://data.bnf.fr/ontology/bnf-onto/"
    xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:foaf="http://xmlns.com/foaf/0.1/"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"

    exclude-result-prefixes="
        xsl bnf-onto dcterms foaf owl
        "
    >

    <xsl:output method="xml" encoding="UTF-8" indent="yes"  />

    <xsl:strip-space elements="*" />

    <!-- La base où se situent les fichiers rameau. -->
    <xsl:param name="base" select="'../../../../files/rameau/'"/>

    <!-- Le dossier source contenant les fichiers xml de Rameau. -->
    <xsl:param name="rameau" select="concat($base, 'source')"/>

    <!-- Les paramètres suivants sont indiqués pour information. -->

    <!-- Le fichier source contenant la liste des fichiers xml de Rameau. -->
    <xsl:param name="source" select="concat($base, 'data/rameau_fichiers.xml')"/>

    <!-- Le fichier de destination des termes. -->
    <xsl:param name="destination" select="concat($base, 'data/rameau_termes.xml')"/>

    <xsl:template match="/">
        <rdf:RDF>
            <xsl:apply-templates select="list/entry"/>
        </rdf:RDF>
    </xsl:template>

    <xsl:template match="entry">
        <xsl:apply-templates select="document(concat($rameau, '/', @name))/rdf:RDF
            /rdf:Description
            [substring(@rdf:about, 1, 22) = 'http://data.bnf.fr/ark']
            [skos:prefLabel]
        "/>
    </xsl:template>

    <xsl:template match="rdf:Description">
        <skos:Concept rdf:about="{substring(@rdf:about, 20)}">
            <skos:prefLabel>
                <xsl:value-of select="skos:prefLabel[@xml:lang = 'fr' or not(@xml:lang) or @xml:lang[1]][1]"/>
            </skos:prefLabel>
            <xsl:apply-templates select="skos:altLabel"/>
            <xsl:apply-templates select="skos:seeAlso"/>
            <xsl:apply-templates select="skos:broader"/>
            <xsl:apply-templates select="skos:narrower"/>
            <xsl:apply-templates select="rdfs:related"/>
        </skos:Concept>
    </xsl:template>

    <xsl:template match="skos:altLabel">
        <skos:altLabel><xsl:value-of select="."/></skos:altLabel>
    </xsl:template>

    <xsl:template match="rdfs:seeAlso">
        <rdfs:seeAlso rdf:resource="{substring(@rdf:resource, 20)}"/>
    </xsl:template>

    <xsl:template match="skos:broader">
        <skos:broader rdf:resource="{substring(@rdf:resource, 20)}"/>
    </xsl:template>

    <xsl:template match="skos:narrower">
        <skos:narrower rdf:resource="{substring(@rdf:resource, 20)}"/>
    </xsl:template>

    <xsl:template match="skos:related">
        <skos:related rdf:resource="{substring(@rdf:resource, 20)}"/>
    </xsl:template>

</xsl:stylesheet>
