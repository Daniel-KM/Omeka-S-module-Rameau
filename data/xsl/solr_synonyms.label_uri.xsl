<?xml version="1.0" encoding="UTF-8"?>
<!--
    Description : Convertit un fichier Skos en une liste de synonymes adaptés à Solr (version 8).

    Utiliser le fichier thesaurus.

    Le synonyme canonique est le descripteur. L’uri est ajoutée pour simplifier
    la configuration.

    @copyright Daniel Berthereau, 2020
    @license CeCILL 2.1 https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"

    exclude-result-prefixes="
        xsl rdf rdfs skos
        "
    >

    <xsl:output method="text" encoding="UTF-8"/>

    <xsl:strip-space elements="*"/>

    <xsl:template match="/rdf:RDF">
        <xsl:apply-templates select="skos:Concept">
            <xsl:sort select="skos:prefLabel/text()"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="skos:Concept[skos:altLabel]">
        <xsl:variable name="prefLabel">
            <xsl:apply-templates select="skos:prefLabel/text()"/>
        </xsl:variable>
        <xsl:variable name="altLabels">
            <xsl:apply-templates select="skos:altLabel/text()"/>
        </xsl:variable>
        <xsl:value-of select="concat($prefLabel, $altLabels, 'https://data.bnf.fr/', @rdf:about, ' => ', $prefLabel, 'https://data.bnf.fr/', @rdf:about, '&#x0A;')"/>
    </xsl:template>

    <xsl:template match="skos:Concept[not(skos:altLabel)]">
        <xsl:variable name="prefLabel">
            <xsl:apply-templates select="skos:prefLabel/text()"/>
        </xsl:variable>
        <xsl:value-of select="concat($prefLabel, 'https://data.bnf.fr/', @rdf:about, ' => ', $prefLabel, 'https://data.bnf.fr/', @rdf:about, '&#x0A;')"/>
    </xsl:template>

    <xsl:template match="skos:prefLabel/text() | skos:altLabel/text()">
        <xsl:call-template name="escape-comma">
            <xsl:with-param name="text" select="."/>
        </xsl:call-template>
    </xsl:template>

    <!-- Protège la virgule, mais l’ajoute en tant que séparateur par simplicité. -->
    <xsl:template name="escape-comma">
        <xsl:param name="text"/>
        <xsl:choose>
            <xsl:when test="contains($text, ',')">
                <xsl:value-of select="substring-before($text, ',')"/>
                <xsl:value-of select="'\,'"/>
                <xsl:call-template name="escape-comma">
                    <xsl:with-param name="text" select="substring-after($text, ',')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($text, ', ')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
