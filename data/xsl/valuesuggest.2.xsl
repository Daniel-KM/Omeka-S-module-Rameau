<?xml version="1.0" encoding="UTF-8"?>
<!--
    Description : Convertit un fichier Skos en une liste de synonymes adaptée à ValueSuggest.

    Utiliser le fichier thesaurus.

    @copyright Daniel Berthereau, 2020
    @license CeCILL 2.1 https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
-->

<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"

    exclude-result-prefixes="
        xsl rdf rdfs skos
        "
    >

    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>

    <xsl:strip-space elements="*"/>

    <!-- Permet de convertir en minuscules non accentuées. -->
    <xsl:variable name="minuscules" select="'aàâæbcçdeéèêëfghiîïjklmnoôöœpqrstuùûüvwxyÿz'"/>
    <xsl:variable name="egal_latin" select="'aaaabccdeeeeefghiiijklmnoooopqrstuuuuvwxyyz'"/>
    <xsl:variable name="alphabet" select="$minuscules"/>
    <xsl:variable name="latin" select="$egal_latin"/>

    <xsl:key name="concept" match="/rdf:RDF/skos:Concept/skos:prefLabel" use="../@rdf:about"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/rdf:RDF">
        <thesaurus>
            <xsl:apply-templates select="@*|node()"/>
        </thesaurus>
    </xsl:template>

    <xsl:template match="skos:Concept">
        <D id="{@rdf:about}">
            <!-- Ce n'est pas un employé pour, mais le libellé principal. Cela
            permet de simplifier les requêtes xpath dans Value Suggest. Le premier
            est le libellé.
            -->
            <L m="{translate(lower-case(key('concept', @rdf:about)), $alphabet, $latin)}">
                <xsl:value-of select="key('concept', @rdf:about)"/>
            </L>
            <xsl:apply-templates select="skos:altLabel"/>
        </D>
    </xsl:template>

    <xsl:template match="skos:altLabel">
        <L m="{translate(lower-case(.), $alphabet, $latin)}">
            <xsl:value-of select="."/>
        </L>
    </xsl:template>

</xsl:stylesheet>
