<?xml version="1.0" encoding="UTF-8"?>
<!--
    Description : Convertit un fichier Rameau de data.bnf.fr en un thesaurus lisible.

    - Uniquement data.bnf (pas les références aux autres thésaurus, par exemple
    http://id.loc.gov/authorities/subjects ou les événements).
    - Choix du label en français, sinon le premier.

    @copyright Daniel Berthereau, 2019-2020
    @license CeCILL 2.1 https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
-->

<xsl:stylesheet version="3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"

    exclude-result-prefixes="
        xsl
        "
    >

    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>

    <xsl:key name="concept" match="/rdf:RDF/skos:Concept/skos:prefLabel" use="../@rdf:about"/>

    <xsl:template match="rdfs:seeAlso|skos:broader|skos:narrower|skos:related">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:value-of select="key('concept', @rdf:resource)"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
