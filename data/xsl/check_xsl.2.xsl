<?xml version="1.0" encoding="UTF-8"?>
<!--
    Description : Permet de vérifier si le processeur xslt gère la version 2.

    @copyright Daniel Berthereau, 2019-2020
    @license CeCILL 2.1 https://cecill.info/licences/Licence_CeCILL_V2.1-fr.txt
-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
