Rameau (module for Omeka S)
===========================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than previous repository.__

Voir le [lisez-moi] en français.

[Rameau] is a module for [Omeka S] that adds the French indexing vocabulary
[RAMEAU] to the list of vocabularies available via the module [Value Suggest].
It creates a file that contains all synonymous ("used for") too , that can be
used with Solr via the module [Search Solr].

Rameau is already included in Value Suggest, but this one is static, so it
doesn't care of connection issues. Furthermore, the display is improved in order
to choose the main term simpler.

This vocabulary is more than a vocabulary. Rameau is an acronym that means
"Répertoire d’autorité-matière encyclopédique et alphabétique unifié", so an
unified encyclopaedic and alphabetical list of subject authorities. It's a
French equivalent of the Library of Congress Subject Headings, so a complete and
very big list of keywords (192000 terms, 2300 MB, 140 MB zipped), that is
largely used in all the Quebecer and French libraries, for French and non-French
books, articles, and any other documents.


Remarks on the license of the vocabulary
----------------------------------------

The vocabulary that is used in this module is licensed under the French
administrative [Open License] of a Prime Minister structure [Etalab], designed
to open data of French administrations.

Nevertheless, there is no clear indication of the version used (references to
the two versions (1.0 and 2.0) can be found, or maybe the user can choose).
Furthermore, the manager of the vocabulary added various conditions, partially
incompatible with the license. Finally, this open data license has not yet been
checked by international organisms (like [FSF] and [OSI]), and has not been
checked against local laws. So, the  vocabulary is free and open source, but not
totally free as in speech, until further clarification by the current manager of
the vocabulary.

So the module does not use the api of the manager of the vocabulary, neither
provides the vocabulary. In lieu of these common ways of access, a dump of it
should be downloaded by the admin and the vocabulary is built on the owned Omeka
server, so the author of the module is not responsible of your usage of the
vocabulary. So check yourself if you can respect the license and the associated
conditions, in particular [here], [and here], and [here too], and probably
somewhere else.


Installation
------------

This module downloads the vocabulary via ftp. The server should have at least
10 GB free space and should be able to download it via this protocol. The
conversion requires an xslt processor be installed first (see below).

This module requires the module [Common] installed first, and, if needed, the
modules [Value Suggest] and [Search Solr].

See general end user documentation for [installing a module].

Uncompress files and rename module folder `Rameau`.

It is recommended to use `proc_open` as a cli strategy in order to get errors
from external commands. Add this in the Omeka file "config/local.config.php":

```php
    'cli' => [
        'execute_strategy' => 'proc_open',
        // ...
    ],
```

The config form of the module displays a button to install the vocabulary. The
xml dump is automatically loaded from the public [ftp server], then unzipped,
then converted into a simplified skos vocabulary, then into a file for Value Suggest
and another for Solr. Files are saved in the main folder "files/rameau/".

### Value Suggest

To use Rameau with Value Suggest, select "BnF: Rameau" as data type in the
property of a resource template.

### Solr

To include the list of synonyms with Solr, replace the default file
"synonyms.txt" by the file "files/rameau/data/synonyms_fr.txt" if the file for
French synonyms does not exists in the Solr config, else replace the latter:

```sh
sudo cp files/rameau/data/synonyms_fr.txt /var/solr/data/omeka/conf/synonyms.txt
sudo chown solr:solr /var/solr/data/omeka/conf/synonyms.txt
sudo chmod 0664 /var/solr/data/omeka/conf/synonyms.txt
```

or

```sh
sudo cp files/rameau/data/synonyms_fr.txt /var/solr/data/omeka/conf/lang/synonyms_fr.txt
sudo chown solr:solr /var/solr/data/omeka/conf/lang/synonyms_fr.txt
sudo chmod 0664 /var/solr/data/omeka/conf/lang/synonyms_fr.txt
```

If you replace the default synonyms file, the config would run automatically.

The list can be used separately too, for exemple for a direct request by subject:
```xml
<fieldType name="rameau" class="solr.TextField" autoGeneratePhraseQueries="true" positionIncrementGap="100">
    <analyzer type="index">
        <tokenizer class="solr.KeywordTokenizerFactory"/>
        <filter class="solr.SynonymGraphFilterFactory" expand="false" ignoreCase="true" synonyms="lang/synonyms_fr.txt" tokenizerFactory="solr.KeywordTokenizerFactory"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
    </analyzer>
    <analyzer type="query">
        <tokenizer class="solr.KeywordTokenizerFactory"/>
        <filter class="solr.SynonymGraphFilterFactory" expand="false" ignoreCase="true" synonyms="lang/synonyms_fr.txt" tokenizerFactory="solr.KeywordTokenizerFactory"/>
    </analyzer>
</fieldType>
<dynamicField name="*_rameau" type="rameau" stored="true" indexed="true"/>
<dynamicField name="*_rameaux" type="rameau" stored="true" indexed="true" multiValued="true"/>
```

After saving, restart Solr and **wait some minutes** for Solr to load the list
of synonyms (30MB), then reindex resources. Tests can be done directly in Solr
user interface with analyser, even if resources are not indexed.

According to Solr configuration, two other xsl sheets are available if needed:

- the default sheet standardizes to the uri only in order to be used for
  [indexation and request];
- the sheet "solr_synonyms_label.xsl" standardizes to the label of the term only;
- the sheet "solr_synonyms_label_uri.xsl" standardizes to label and uri.

**Warning** : The synonyms file size is some dozens of MB, so Solr needs a big
size of ram of swap, in particular when multiple Solr cores use it. See below to
increase the swap.


Config of the xslt processor
----------------------------

Xslt has two main versions:  xslt 1.0 and xslt 2.0. The first is often installed
by default with php via the extension `php-xsl` or the package `php7-xsl`,
depending on your system. It is until ten times slower than xslt 2.0 and sheets
are more complex to write.

So it’s recommended to install an xslt 2 processor, that can process xslt 1.0
and xslt 2.0 sheets. Anyway, it is required when the memory is small, because
the xml sources are very big.

The command depends on your server. Use "{xsl}", "{source}", and "{destination}"
for the the stylesheet, the xml source file, and the output. Here are examples
with the most common free xslt 2 processor, saxon. Because it is a Java tool, a
JRE should be installed, for example `openjdk-11-jre-headless` or upper.

Examples for Debian 6+ / Ubuntu / Mint (with the package "libsaxonb-java"):
```sh
saxonb-xslt -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Examples for Debian 8+ / Ubuntu / Mint (with the package "libsaxonhe-java"):
```sh
CLASSPATH=/usr/share/java/Saxon-HE.jar java net.sf.saxon.Transform -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Example for Fedora / RedHat / Centos / Mandriva / Mageia (package "saxon"):
```sh
java -cp /usr/share/java/saxon.jar net.sf.saxon.Transform -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Or with packages "saxon", "saxon-scripts", "xml-commons-apis" and "xerces-j2":
```sh
saxon -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Specific options can be added, in particular memory and warnings (unavailable on
some java versions):
```sh
# Debian
CLASSPATH=/usr/share/java/Saxon-HE.jar nice -n 19 java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:{xsl} -s:{source} -o:{destination}
```

Note: With Saxon, warnings are processed as errors. That’s why the parameter
`-warnings:silent` is important to be able to process an import with a bad xsl
sheet. It can be removed with default correct xsl sheets that doesn’t warn
anything.

Furthermore, Saxon needs a lot of memory in ram. If there are not enough ram,
the swap must be large, about 40GB, for the first process.

To increase swap temporarily:

```sh
df -h
sudo dd if=/dev/zero of=/swap bs=1M count=40000
sudo chmod 600 /swap
sudo mkswap /swap
sudo swapon /swap
swapon -s
free -h
```

If you cannot create the files automatically, here are commands to run manually
(Debian):

```sh
cd /var/www/html/Omeka

mkdir -p files/rameau/source files/rameau/data files/temp
wget 'ftp://databnf:databnf@pef.bnf.fr/DATA/databnf_rameau_xml.tar.gz' -O 'files/temp/databnf_rameau_xml.tar.gz'
tar -xf ./files/temp/databnf_rameau_xml.tar.gz -C ./files/rameau/source/

echo '<?xml version="1.0" encoding="UTF-8"?>' > files/rameau/data/fichiers.xml
echo '<list>' >> files/rameau/data/fichiers.xml
for f in $( ls files/rameau/source | sort ); do echo -e "    <entry name=\"$f\"/>" >> files/rameau/data/fichiers.xml ; done
echo '</list>' >> files/rameau/data/fichiers.xml

CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/rameau_skos.xsl' -s:'files/rameau/data/fichiers.xml' -o:'files/rameau/data/skos.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/thesaurus.xsl' -s:'files/rameau/data/skos.xml' -o:'files/rameau/data/thesaurus.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/valuesuggest.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/valuesuggest_rameau.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.uri.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.uri.txt'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.label.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.label.txt'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.label_uri.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.label_uri.txt'

chown -R www-data:www-data files/rameau
```

The whole process takes some minutes on a standard server.

Currently, the original files of Rameau are updated one or two times a year.


TODO
----

- [ ] Use XmlReader for the creation of the skos files to avoid Saxon.
- [ ] Create an output for the [automatically managed synonyms](https://lucene.apache.org/solr/guide/8_7/filter-descriptions.html#managed-synonym-graph-filter) in Solr.
- [ ] FIll in synonyms file only the synonymes that are present in the database (and update it via the automatic management of synonyms).
- [ ] Create the Rameau hierarchic tree or integrate another thesaurus tool.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [module issues] page on GitLab.


License
-------

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.

See [above] for the license of the vocabulary.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2024 (see [Daniel-KM] on GitLab)

These features are built for the digital library [Manioc] of the Université des
Antilles and Université de la Guyane, previously managed with [Greenstone].


[Rameau]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau
[lisez-moi]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau/blob/master/LISEZMOI.md
[Omeka S]: https://omeka.org/s
[RAMEAU]: http://rameau.bnf.fr
[Common]: https://gitlab.com/Daniel-KM/Omeka-S-module-Common
[Value Suggest]: https://github.com/omeka-s-modules/ValueSuggest
[Search Solr]: https://gitlab.com/Daniel-KM/Omeka-S-module-SearchSolr
[Open License]: https://www.etalab.gouv.fr/licence-ouverte-open-licence
[Etalab]: https://www.etalab.gouv.fr
[here]: https://www.bnf.fr/sites/default/files/2018-11/metadonnees_bnf_cond_utilisation.pdf
[and here]: https://www.bnf.fr/fr/conditions-de-reutilisations-des-donnees-de-la-bnf
[here too]: https://data.bnf.fr/licence
[installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[indexation and request]: https://lucene.apache.org/solr/guide/8_6/filter-descriptions.html#synonym-graph-filter
[module issues]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[MIT]: https://github.com/sandywalker/webui-popover/blob/master/LICENSE.txt
[above]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau#remarks-on-the-license-of-the-vocabulary
[Manioc]: http://www.manioc.org
[Greenstone]: http://www.greenstone.org
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
