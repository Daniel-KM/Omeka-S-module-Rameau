<?php declare(strict_types=1);

namespace Rameau\Service;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Rameau\DataType\Rameau\Rameau;

class RameauDataTypeFactory implements FactoryInterface
{
    protected $types = [
        'valuesuggest:bnf:rameau' => [
            'label' => 'BnF: Rameau', // @translate
        ],
    ];

    public function __invoke(ContainerInterface $services, $requestedName, array $options = null)
    {
        $dataType = new Rameau($services);
        $dataType
            ->setRameauName($requestedName)
            ->setRameauLabel($this->types[$requestedName]['label']);

        $basePath = $services->get('Config')['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');
        $xml = $basePath . '/rameau/data/valuesuggest_rameau.xml';
        if (file_exists($xml)) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->load($xml);
            $xpath = new \DOMXPath($doc);
            $dataType->setRameauXpath($xpath);
        }

        return $dataType;
    }
}
