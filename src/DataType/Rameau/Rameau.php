<?php declare(strict_types=1);

namespace Rameau\DataType\Rameau;

use Rameau\Suggester\Rameau\Xsl;
use ValueSuggest\DataType\AbstractDataType;

class Rameau extends AbstractDataType
{
    protected $rameauName;
    protected $rameauLabel;
    protected $xpath;

    public function setRameauName($rameauName)
    {
        $this->rameauName = $rameauName;
        return $this;
    }

    public function setRameauLabel($rameauLabel)
    {
        $this->rameauLabel = $rameauLabel;
        return $this;
    }

    public function setRameauXpath($xpath)
    {
        $this->xpath = $xpath;
        return $this;
    }

    public function getSuggester()
    {
        return new Xsl($this->xpath);
    }

    public function getName()
    {
        return $this->rameauName;
    }

    public function getLabel()
    {
        return $this->rameauLabel;
    }
}
