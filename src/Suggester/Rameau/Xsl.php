<?php declare(strict_types=1);

namespace Rameau\Suggester\Rameau;

use DOMXPath;
use ValueSuggest\Suggester\SuggesterInterface;

class Xsl implements SuggesterInterface
{
    /**
     * @var DOMXPath|null
     */
    protected $xpath;

    public function __construct(?DOMXPath $xpath = null)
    {
        $this->xpath = $xpath;
    }

    public function getSuggestions($query, $lang = null)
    {
        if (!$this->xpath) {
            return [];
        }

        $query = htmlspecialchars(
            preg_replace('#[\x00-\x08\x0B\x0C\x0E-\x1F]+#', '', $query),
            ENT_QUOTES | ENT_XML1
        );

        $query = $this->lowerTransliterate($query);

        $xpathQuery = "/thesaurus/D[L[contains(@m, '$query')]][position() <= 500]";

        $nodeList = $this->xpath->query($xpathQuery);
        if (!$nodeList || !$nodeList->length) {
            return [];
        }

        // Parse the xpath response.
        $suggestions = [];
        foreach ($nodeList as $node) {
            $suggestions[] = [
                'value' => $node->getElementsByTagName('L')[0]->textContent,
                'data' => [
                    'uri' => 'https://data.bnf.fr/' . $node->getAttribute('id'),
                    'info' => $this->alternatives($node->getElementsByTagName('L')),
                ],
            ];
        }

        return $suggestions;
    }

    protected function lowerTransliterate(string $string): string
    {
        $string = mb_strtolower($string);

        if (extension_loaded('intl')) {
            $transliterator = \Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: NFC;');
            return $transliterator->transliterate($string);
        }

        if (extension_loaded('iconv')) {
            $trans = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
            return $trans ?: $string;
        }

        $mapping = [
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            // TODO Improve See xsl conversion: single character substitution.
            // 'æ' => 'ae',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            // 'œ' => 'oe',
            'œ' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'u',
            'ý' => 'y',
        ];
        return str_replace(array_keys($mapping), array_values($mapping), $string);
    }

    protected function alternatives($nodeList)
    {
        $output = [];
        foreach ($nodeList as $node) {
            $output[] = $node->textContent;
        }
        return implode(' / ', $output);
    }
}
