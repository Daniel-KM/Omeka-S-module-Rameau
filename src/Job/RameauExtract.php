<?php declare(strict_types=1);

namespace Rameau\Job;

class RameauExtract extends AbstractCheck
{
    /**
     * @var \Laminas\Log\Logger
     */
    protected $logger;

    /**
     * @var \Omeka\Stdlib\Cli
     */
    protected $cli;

    /**
     * @var string
     */
    protected $xslCommand;

    /**
     * @var int
     */
    protected $xslVersion;

    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $baseXsl;

    /**
     * @var string
     */
    protected $sourceXml;

    /**
     * @var string
     */
    protected $sourceData;

    /**
     * Default options for output (tsv).
     *
     * @var array
     */
    protected $options = [
        'delimiter' => "\t",
        'enclosure' => 0,
        'escape' => 0,
    ];

    /**
     * List of columns keys and labels for the output spreadsheet.
     *
     * @var array
     */
    protected $columns = [
        'heading' => 'Heading', // @translate
        'synonymous' => 'Synonymous', // @translate
    ];

    /**
     * Supported conversion formats.
     *
     * @var array
     */
    protected $formats = [
        'value_suggest' => [
            'xsl' => 'valuesuggest',
            'destination' => 'valuesuggest_rameau.xml',
        ],
        'solr_synonyms_label' => [
            'xsl' => 'solr_synonyms.label',
            'destination' => 'synonyms_fr.label.txt',
        ],
        'solr_synonyms_uri' => [
            'xsl' => 'solr_synonyms.uri',
            'destination' => 'synonyms_fr.uri.txt',
        ],
        'solr_synonyms_label_uri' => [
            'xsl' => 'solr_synonyms.label_uri',
            'destination' => 'synonyms_fr.label_uri.txt',
        ],
    ];

    public function perform(): void
    {
        $services = $this->getServiceLocator();

        // The reference id is the job id for now.
        $referenceIdProcessor = new \Laminas\Log\Processor\ReferenceId();
        $referenceIdProcessor->setReferenceId('rameau/job_' . $this->job->getId());

        $this->logger = $services->get('Omeka\Logger');
        $this->logger->addProcessor($referenceIdProcessor);
        $this->cli = $services->get('Omeka\Cli');

        $this->basePath = $this->config['file_store']['local']['base_path'] ?: (OMEKA_PATH . '/files');
        $this->baseUrl = $this->config['file_store']['local']['base_uri'] ?: $this->getArg('base_path') . '/files';

        $this->baseXsl = dirname(__DIR__, 2) . '/data/xsl';
        $this->sourceXml = $this->basePath . '/rameau/source';
        $this->sourceData = $this->basePath . '/rameau/data';

        $this->xslCommand = $services->get('Omeka\Settings')->get('rameau_xslt_processor');
        if (empty($this->xslCommand)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'Xslt command is not defined.' // @translate
            );
            return;
        }

        if (!in_array($this->getArg('source'), ['unzipped', 'skos', 'thesaurus'])) {
            $this->checkFreeSpace();
        }

        $this->xslVersion();

        if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
            return;
        }

        $this->logger->info(
            'Process init.' // @translate
        );

        // Don't use the omeka/sys temp dir, it's big and may be reused or
        // manually prepared. Else see ExtractOcr for an example.
        $destination = $this->basePath . '/temp';
        if (!$this->createDir($destination)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'Unable to prepare directory "{path}". Check rights.', // @translate
                ['path' => 'files/temp']
            );
            return;
        }

        // Purge the existing extraction.
        $destination = $this->basePath . '/rameau';
        if (file_exists($destination)) {
            if ($this->getArg('remove_existing_files')) {
                if (in_array($this->getArg('source'), ['unzipped', 'skos', 'thesaurus'])) {
                    $this->removeDir($destination . '/data', true);
                } else {
                    $this->removeDir($destination, true);
                }
            } elseif (!in_array($this->getArg('source'), ['unzipped', 'skos', 'thesaurus'])) {
                $this->removeDir($destination . '/source', true);
            }
        }
        // Create the subdir directly now.
        if (!$this->createDir($this->sourceXml)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'Unable to prepare directory "{path}". Check rights.', // @translate
                ['path' => 'files/rameau']
            );
            return;
        }
        $this->createDir($this->sourceData);

        // $this->initializeOutput();
        if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
            return;
        }

        $this->process();

        $this->logger->notice(
            'Process completed.' // @translate
        );

        // $this->finalizeOutput();
    }

    protected function process(): void
    {
        if (!in_array($this->getArg('source'), ['unzipped', 'skos', 'thesaurus'])) {
            $this->prepareFiles();
        }
        if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
            return;
        }
        if (!in_array($this->getArg('source'), ['skos', 'thesaurus'])) {
            $this->prepareXmlListFiles();
            if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
                return;
            }
            $this->extractTerms();
            if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
                return;
            }
        }
        if (!in_array($this->getArg('source'), ['thesaurus'])) {
            $this->extractThesaurus();
            if ($this->job->getStatus() === \Omeka\Entity\Job::STATUS_ERROR) {
                return;
            }
        }
        foreach ($this->getArg('formats', []) as $format) {
            $this->extractFormat($format);
        }
    }

    protected function extractTerms(): void
    {
        $xsl = $this->baseXsl . '/rameau_skos.xsl';
        $source = $this->sourceData . '/fichiers.xml';
        $destination = $this->sourceData . '/skos.xml';
        $result = $this->executeXsl($xsl, $source, $destination);
        if (!$result) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'An issue occurred during extraction of terms.' // @translate
            );
            return;
        }

        $this->logger->notice(
            'The terms were extracted in {url}.', // @translate
            ['url' => $this->baseUrl . '/rameau/data/skos.xml']
        );
    }

    protected function extractThesaurus(): void
    {
        $xsl = $this->baseXsl . ($this->xslVersion === 3 ? '/thesaurus.3.xsl' : '/thesaurus.xsl');
        $source = $this->sourceData . '/skos.xml';
        $destination = $this->sourceData . '/thesaurus.xml';
        $result = $this->executeXsl($xsl, $source, $destination);
        if (!$result) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'An issue occurred during extraction of the thesaurus.' // @translate
            );
            return;
        }

        $this->logger->notice(
            'The thesausus was extracted in {url} (size: {size}MB).', // @translate
            ['url' => $this->baseUrl . '/rameau/data/thesaurus.xml', 'size' => ceil(filesize($destination) / 1000000)]
        );
    }

    protected function extractFormat($format): void
    {
        $base = $this->baseXsl . '/' . $this->formats[$format]['xsl'];
        $xsl = $base . ($this->xslVersion >= 2 && file_exists($base . '.2.xsl') ? '.2.xsl' : '.xsl');
        $source = $this->sourceData . '/' . ($this->formats[$format]['source'] ?? 'thesaurus.xml');
        $destination = $this->sourceData . '/' . $this->formats[$format]['destination'];
        $result = $this->executeXsl($xsl, $source, $destination);
        if (!$result) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'An issue occurred during extraction of the data for format "{format}".', // @translate
                ['format' => $format]
            );
            return;
        }

        $this->logger->notice(
            'The data for format "{format}" were extracted in {url} (size: {size}MB).', // @translate
            ['format' => $format, 'url' => $this->baseUrl . '/rameau/data/' . $this->formats[$format]['destination'], 'size' => ceil(filesize($destination) / 1000000)]
        );
    }

    protected function executeXsl($xsl, $source, $destination): bool
    {
        $cmd = str_replace(['{xsl}', '{source}', '{destination}'], [escapeshellarg($xsl), escapeshellarg($source), escapeshellarg($destination)], $this->xslCommand);
        $result = $this->cli->execute($cmd);
        if (!empty($result)) {
            $this->logger->notice(
                'Unzip output: {result}', // @translate
                ['output' => implode(PHP_EOL, $result)]
            );
        }
        return $result !== false;
    }

    protected function checkFreeSpace(): void
    {
        $availableSize = disk_free_space($this->basePath);
        $minSize = 10000000000;
        if ($availableSize < $minSize) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'A minimum size of {min_size}MB is required in the destination dir on the server, only {size}MB are available.', // @translate
                ['min_size' => ceil($minSize / 1000000), 'size' => ceil($availableSize / 1000000)]
            );
        }
    }

    protected function xslVersion(): void
    {
        $xsl = $this->baseXsl . '/check_xsl.3.xsl';
        $source = $this->baseXsl . '/check_xsl.xml';
        $destination = @tempnam(sys_get_temp_dir(), 'omk_rameau_');
        $cmd = str_replace(['{xsl}', '{source}', '{destination}'], [escapeshellarg($xsl), escapeshellarg($source), escapeshellarg($destination)], $this->xslCommand);
        $output = [];
        $returnVar = null;
        exec($cmd, $output, $returnVar);
        @unlink($destination);
        if ($returnVar === 0) {
            $this->xslVersion = 3;
            return;
        }
        $xsl = $this->baseXsl . '/check_xsl.2.xsl';
        $cmd = str_replace(['{xsl}', '{source}', '{destination}'], [escapeshellarg($xsl), escapeshellarg($source), escapeshellarg($destination)], $this->xslCommand);
        exec($cmd, $output, $returnVar);
        @unlink($destination);
        $this->xslVersion = $returnVar === 0 ? 2 : 1;
    }

    protected function prepareXmlListFiles(): void
    {
        // Since the ftp server is not secure, a check is done on the files.
        $allFiles = $this->listFilesInFolder($this->sourceXml);
        $files = $this->listFilesInFolder($this->sourceXml, false, ['xml']);
        if (count($files) !== count($allFiles)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'For security, there must be only xml files inside the source. Check the source file {file}.', // @translate
                ['file' => 'files/temp/databnf_rameau_xml.tar.gz']
            );
            return;
        }
        if (!count($files)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'There is no xml file in the zipped file. Check the source file {file}.', // @translate
                ['file' => 'files/temp/databnf_rameau_xml.tar.gz']
            );
            return;
        }

        $xml = "\n";
        foreach ($files as $file) {
            $xml .= sprintf('    <entry name="%s"/>', $file) . "\n";
        }
        $xml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<list>$xml</list>

XML;
        $result = file_put_contents($this->sourceData . '/fichiers.xml', $xml);
        if (!$result) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'Unable to save the list of files. Check the rights in {dir}.', // @translate
                ['dir' => 'files/rameau/data']
            );
            // return;
        }
    }

    protected function prepareFiles(): void
    {
        $url = 'ftp://databnf:databnf@pef.bnf.fr/DATA/databnf_rameau_xml.tar.gz';
        $basename = basename($url);
        $destinationZip = $this->basePath . '/temp/' . $basename;

        $sourceProcess = $this->getArg('source');
        if ($sourceProcess === 'download' || !file_exists($destinationZip)) {
            $result = file_put_contents($destinationZip, fopen($url, 'rb'));
            if (!$result) {
                $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
                $this->logger->err(
                    'An issue occurred during downloading.' // @translate
                );
                return;
            }
            $this->logger->notice(
                'The zipped source file of {size}MB was downloaded.', // @translate
                ['size' => ceil($result / 1000000)]
            );
        }

        // php doesn't know how to untar a file natively, only gz.
        if (!$this->unzipFileToDir($destinationZip, $this->sourceXml)) {
            if (!$result) {
                $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
                $this->logger->err(
                    'An issue occurred during unzip.' // @translate
                );
                return;
            }
        }

        $result = $this->dirSize($this->sourceXml);
        $this->logger->notice(
            'Size of unzipped files is {size}MB.', // @translate
            ['size' => ceil($result / 1000000)]
        );

        $files = $this->listFilesInFolder($this->sourceXml);
        if (!count($files)) {
            $this->job->setStatus(\Omeka\Entity\Job::STATUS_ERROR);
            $this->logger->err(
                'There is no file in the zipped file. Check the source file {file}.', // @translate
                ['file' => 'files/temp/databnf_rameau_xml.tar.gz']
            );
            return;
        }

        if ($this->getArg('remove_downloaded_file')) {
            $result = @unlink($destinationZip);
            if ($result) {
                $this->logger->err(
                    'The downloaded file was removed.' // @translate
                );
            } else {
                $this->logger->warn(
                    'The downloaded file cannot be removed.' // @translate
                );
            }
        }
    }

    protected function createDir($path): bool
    {
        return file_exists($path)
            ? is_dir($path) && is_writeable($path)
            : @mkdir($path, 0775, true);
    }

    protected function unzipFileToDir($source, $destination): bool
    {
        $command = 'tar';
        $command = (string) $this->cli->getCommandPath($command);

        $source = escapeshellarg($source);
        $destination = escapeshellarg($destination);
        $command = "$command -xf $source -C $destination";

        $result = $this->cli->execute($command);
        if (!empty($result)) {
            $this->logger->warn(
                'Unzip output: {result}', // @translate
                ['output' => $result]
            );
        }
        return $result !== false;
    }

    /**
     * Checks and removes a folder recursively.
     *
     * @see \ArchiveRepertory\File\FileWriter::removeDir()
     *
     * @param string $path Full path of the folder to remove.
     * @param bool $evenNonEmpty Remove non empty folder. This parameter can be
     * used with non standard folders.
     * @return bool
     */
    protected function removeDir($path, $evenNonEmpty = false): bool
    {
        $path = realpath((string) $path);
        if ($path
            && mb_strlen($path)
            && $path != DIRECTORY_SEPARATOR
            && file_exists($path)
            && is_dir($path)
            && is_readable($path)
            && is_writeable($path)
            && ($evenNonEmpty || count(array_diff(@scandir($path), ['.', '..'])) == 0)
        ) {
            return $this->recursiveRemoveDir($path);
        }
        return false;
    }

    /**
     * Removes directories recursively.
     *
     * @param string $dirPath Directory name.
     * @return bool
     */
    protected function recursiveRemoveDir($dirPath): bool
    {
        $files = array_diff(scandir((string) $dirPath), ['.', '..']);
        foreach ($files as $file) {
            $path = $dirPath . DIRECTORY_SEPARATOR . $file;
            if (is_dir($path)) {
                $this->recursiveRemoveDir($path);
            } else {
                unlink($path);
            }
        }
        return rmdir((string) $dirPath);
    }

    /**
     * Get a relative or full path of files filtered by extensions recursively
     * in a directory.
     *
     * @param string $dir
     * @param bool $absolute
     * @param string $extensions
     * @return array
     */
    protected function listFilesInFolder($dir, $absolute = false, array $extensions = []): array
    {
        if (empty($dir) || !file_exists($dir) || !is_dir($dir) || !is_readable($dir)) {
            return [];
        }
        $regex = empty($extensions)
            ? '/^.+$/i'
            : '/^.+\.(' . implode('|', $extensions) . ')$/i';
        $directory = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new \RecursiveIteratorIterator($directory);
        $regex = new \RegexIterator($iterator, $regex, \RecursiveRegexIterator::GET_MATCH);
        $files = [];
        if ($absolute) {
            foreach ($regex as $file) {
                $files[] = reset($file);
            }
        } else {
            $dirLength = mb_strlen($dir) + 1;
            foreach ($regex as $file) {
                $files[] = mb_substr(reset($file), $dirLength);
            }
        }
        sort($files);
        return $files;
    }

    protected function dirSize($dir): int
    {
        $size = 0;
        $dir = realpath((string) $dir);
        if ($dir !== false && mb_strlen($dir) && $dir !== DIRECTORY_SEPARATOR && file_exists($dir) && is_dir($dir) && is_readable($dir)) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS)) as $file) {
                $size += $file->getSize();
            }
        }
        return $size;
    }
}
