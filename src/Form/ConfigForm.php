<?php declare(strict_types=1);

namespace Rameau\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class ConfigForm extends Form
{
    public function init(): void
    {
        $this
            ->add([
                'name' => 'rameau_xslt_processor',
                'type' => Element\Text::class,
                'options' => [
                    'label' => 'Command for xslt processor', // @translate
                    'documentation' => 'https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau#Install',
                ],
                'attributes' => [
                    'id' => 'rameau_xslt_processor',
                ],
            ])
            ->add([
                'name' => 'source',
                'type' => Element\Radio::class,
                'options' => [
                    'label' => 'Source', // @translate
                    'value_options' => [
                        'download' => 'Download', // @translate
                        'local' => 'Use file files/temp/databnf_rameau_xml.tar.gz', // @translate
                        'unzipped' => 'Use the unzipped files inside files/rameau/source/', // @translate
                        'skos' => 'Use the extracted skos', // @translate
                        'thesaurus' => 'Use the extracted thesaurus', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'source',
                    'value' => 'download',
                ],
            ])
            ->add([
                'name' => 'formats',
                'type' => Element\MultiCheckbox::class,
                'options' => [
                    'label' => 'Formats', // @translate
                    'value_options' => [
                        'value_suggest' => 'Value Suggest', // @translate
                        'solr_synonyms_uri' => 'Solr synonyms (into uri)', // @translate
                        'solr_synonyms_label' => 'Solr synonyms (into label)', // @translate
                        'solr_synonyms_label_uri' => 'Solr synonyms (into label and uri)', // @translate
                    ],
                ],
                'attributes' => [
                    'id' => 'formats',
                ],
            ])
            ->add([
                'name' => 'remove_downloaded_file',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Remove downloaded file', // @translate
                ],
                'attributes' => [
                    'id' => 'remove_downloaded_file',
                ],
            ])
            ->add([
                'name' => 'remove_existing_files',
                'type' => Element\Checkbox::class,
                'options' => [
                    'label' => 'Remove existing files in files/rameau/data', // @translate
                ],
                'attributes' => [
                    'id' => 'remove_existing_files',
                ],
            ])
            ->add([
                'name' => 'process',
                'type' => Element\Submit::class,
                'options' => [
                    'label' => 'Download and extract synonymous from Rameau vocabulary', // @translate
                    'info' => 'This process needs some minutes. Check results of the job.', // @translate
                ],
                'attributes' => [
                    'id' => 'process',
                    'value' => 'Process', // @translate
                ],
            ]);

        $this->getInputFilter()
            ->add([
                'name' => 'formats',
                'required' => false,
            ]);
    }
}
