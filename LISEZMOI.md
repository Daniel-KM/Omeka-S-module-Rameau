Rameau (module pour Omeka S)
============================

> __Les nouvelles versions de ce module et l’assistance pour Omeka S version 3.0
> et supérieur sont disponibles sur [GitLab], qui semble mieux respecter les
> utilisateurs et la vie privée que le précédent entrepôt.__

See [English readme].

[Rameau] est un module pour [Omeka S] qui ajoute le vocabulaire d’indexation
francophone [RAMEAU] à la liste des vocabulaires disponibles via le module [Value Suggest]
et qui permet d’utiliser les descripteurs synonymes ("employé pour"), qui peuvent être
utilisé avec Solr via le module [Search Solr].

Rameau est déjà inclus dans Value Suggest, mais celui-ci est statique, donc il
n’a pas de problèmes de connexion. En outre, l’affichage indique la liste des
alternatives.

Ce vocabulaire est plus qu’un vocabulaire. Rameau est un acronyme qui signifie
"Répertoire d’autorité-matière encyclopédique et alphabétique unifié". Il s’agit
d’un équivalent français des vedettes-matières Bibliothèque du Congrès et il
contient une très grande liste de mots-clés (192 000 termes, 2 300 Mo, 140 Mo
compressés). Il est largement utilisé dans toutes les bibliothèques québécoises
et françaises, pour les livres, articles et tout autre document, français ou non.


Remarques sur la licence du vocabulaire
---------------------------------------

Le vocabulaire utilisé dans ce module est autorisé par la [Licence ouverte], qui
est une licence administrative gérée par une structure du Premier ministre [Etalab]
et conçue pour ouvrir les données des administrations françaises.

Cependant, il n’y a pas d’indication claire de la version utilisée (les
références aux deux versions (1.0 et 2.0) peuvent être trouvées, ou peut-être
l’utilisateur peut-il choisir). En outre, le gestionnaire du vocabulaire a
ajouté diverses conditions, partiellement incompatibles avec la licence. Par
ailleurs, cette licence ouverte n’a pas encore été vérifiée par les organismes
internationaux (tel que la [FSF] et l’[OSI]), et n’a pas été validé par rapport
aux lois locales. Ainsi, le vocabulaire est gratuit et _open source_, mais pas
totalement libre, tant que cela n’est pas clarifé par le gestionnaire actuel du
vocabulaire.

Le module n’utilise donc pas l’api du gestionnaire de vocabulaire et ne fournit
pas le vocabulaire. À la place de ces moyens d’accès habituels, le vocabulaire
est téléchargé par l’admin et le vocabulaire est reconstruit sur son propre
serveur Omeka, de sorte que l’auteur du module n’est pas responsable de
l’utilisation qui est faite du vocabulaire. Vérifiez donc vous-même si vous
pouvez respecter la licence et les conditions associées, en particulier [ici],
[et ici], et [ici aussi], et sans doute ailleurs.


Installation
------------

Ce module permet de télécharger le vocabulaire par ftp. Le serveur doit disposer
d’au moins 10 Go d’espace libre et doit pouvoir effectuer un téléchargement via
ce protocole. La conversion nécessite l’installation préalable d’un processeur
xslt (voir ci-dessous).

Ce module nécessite le module [Common] installé en premier et, si besoin, les
modules [Value Suggest] et [Search Solr].

Voir la documentation générale pour l’utilisateur final pour l’[installation d’un module].

Décompressez les fichiers et renommez le dossier du module `Rameau`.

Il est recommandé d’utiliser `proc_open` comme stratégie cli afin d’obtenir des erreurs
à partir de commandes externes. Ajoutez ceci dans le fichier Omeka "config/local.config.php" :

```php
    'cli' => [
        'execute_strategy' => 'proc_open',
        // ...
    ],
```

Le formulaire de configuration du module affiche un bouton pour installer le
vocabulaire. Le fichier xml du vocabulaire est automatiquement chargé à partir
du serveur public [ftp], puis décompressé, puis converti en un vocabulaire skos
simplifié, puis dans un fichier pour Value Suggest et un autre pour Solr. Les
fichiers sont enregistrés dans le dossier principal "files/rameau/".

### Value Suggest

Pour utiliser Rameau avec Value Suggest, sélectionnez "BnF : Rameau" en tant que
type de données d’une propriété d’un modèle de ressources.

### Solr

Pour inclure la liste des synonymes avec Solr, remplacez le fichier par défaut
"synonyms.txt" par le fichier "files/rameau/data/synonyms_fr.txt" si le fichier
des synonymes français n’existe pas dans la configuration Solr, sinon remplacez
ce dernier :

```sh
sudo cp files/rameau/data/synonyms_fr.txt /var/solr/data/omeka/conf/synonyms.txt
sudo chown solr:solr /var/solr/data/omeka/conf/synonyms.txt
sudo chmod 0664 /var/solr/data/omeka/conf/synonyms.txt
```

ou

```sh
sudo cp files/rameau/data/synonyms_fr.txt /var/solr/data/omeka/conf/lang/synonyms_fr.txt
sudo chown solr:solr /var/solr/data/omeka/conf/lang/synonyms_fr.txt
sudo chmod 0664 /var/solr/data/omeka/conf/lang/synonyms_fr.txt
```

Si vous remplacez le fichier des synonymes par défaut, la configuration devrait
fonctionner automatiquement.

Il est également possible d’utiliser la liste séparément, par exemple pour une
interrogation directe par sujet :
```xml
<fieldType name="rameau" class="solr.TextField" autoGeneratePhraseQueries="true" positionIncrementGap="100">
    <analyzer type="index">
        <tokenizer class="solr.KeywordTokenizerFactory"/>
        <filter class="solr.SynonymGraphFilterFactory" expand="false" ignoreCase="true" synonyms="lang/synonyms_fr.txt" tokenizerFactory="solr.KeywordTokenizerFactory"/>
        <filter class="solr.FlattenGraphFilterFactory"/>
    </analyzer>
    <analyzer type="query">
        <tokenizer class="solr.KeywordTokenizerFactory"/>
        <filter class="solr.SynonymGraphFilterFactory" expand="false" ignoreCase="true" synonyms="lang/synonyms_fr.txt" tokenizerFactory="solr.KeywordTokenizerFactory"/>
    </analyzer>
</fieldType>
<dynamicField name="*_rameau" type="rameau" stored="true" indexed="true"/>
<dynamicField name="*_rameaux" type="rameau" stored="true" indexed="true" multiValued="true"/>
```

Après enregistrement, redémarrer Solr et **patientez quelques minutes** le temps
de prendre en compte la liste des synonymes (30 Mo), puis réindexez vos
ressources. Les tests peuvent être réalisés directement dans l’interface Solr
avec l’analyseur, indépendamment des ressources.

En fonction de la configuration de Solr, deux autres conversions sont possible :

- la feuille par défaut "solr_synonyms.uri.xsl" normalise vers l’uri seulement afin
  de permettre son utilisation en [indexation et en requête] ;
- la feuille "solr_synonyms.label.xsl" normalise vers le libellé seulement ;
- la feuille "solr_synonyms.label_uri.xsl" normalise vers le libellé et l’uri.

**Attention** : Le fichier des synonymes représentant plusieurs dizaines de Mo,
Solr a besoin d’une grande quantité de ram ou d’espace d’échange (_swap_),
surtout si plusieurs cœurs Solr l’utilisent. Cf. ci-dessous pour élargir la
mémoire d'échange.


Configuration du processeur xslt
--------------------------------

Xslt a deux versions principales : xslt 1.0 et xslt 2.0. La première est souvent
installée par défaut avec php via l’extension `php-xsl` ou le paquet `php7-xsl`,
en fonction de votre système. Il est jusqu’à dix fois plus lent que xslt 2.0 et
les feuilles sont plus complexes à écrire.

Il est donc recommandé d’installer un processeur xslt 2, qui peut traiter les
feuilles xslt 1.0 et xslt 2.0. Quoi qu’il en soit, cet outil est nécessaire
lorsque la mémoire est petite, car les sources xml sont très grosses.

La commande dépend de votre serveur. Utilisez "{xsl}", "{source}" et "{destination}"
pour la feuille de style, le fichier source xml et la sortie. Ci-dessous sont
indiqués des exemples avec le processeur libre xslt 2 le plus courant, Saxon.
Comme il s’agit d’un outil Java, un JRE doit être installé, par exemple `openjdk-11-jre-headless`.
ou supérieur.

Exemples pour Debian 6+ / Ubuntu / Mint (avec le paquet "libsaxonb-java") :
```sh
saxonb-xslt -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Exemples pour Debian 8+ / Ubuntu / Mint (avec le paquet "libsaxonhe-java"):
```sh
CLASSPATH=/usr/share/java/Saxon-HE.jar java net.sf.saxon.Transform -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Exemples pour Fedora / RedHat / Centos / Mandriva / Mageia (paquet "saxon"):
```sh
java -cp /usr/share/java/saxon.jar net.sf.saxon.Transform -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Ou avec les paquest "saxon", "saxon-scripts", "xml-commons-apis" et "xerces-j2":
```sh
saxon -ext:on -versionmsg:off -warnings:silent -xsl:{xsl} -s:{source} -o:{destination}
```

Des options particulières peuvent être ajoutées, en particulier la mémoire et
messages (indisponible sur certaines versions de java) :
```sh
# Debian
CLASSPATH=/usr/share/java/Saxon-HE.jar nice -n 19 java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:{xsl} -s:{source} -o:{destination}
```

Note : Avec Saxon, les avertissements sont traités comme des erreurs. C’est
pourquoi le paramètre `-warnings:silent` est important pour pouvoir traiter un
import avec une feuille xsl incorrecte. Il peut être supprimé avec des feuilles
xsl par défaut.

De plus, Saxon a besoin de beaucoup de mémoire en ram. S’il n’y a pas assez de
mémoire vive, le fichier d’échange (_swap_) doit être assez important, environ
40 Go, pour le premier traitement.

Pour agrandir le fichier d’échange temporairement :

```sh
df -h
sudo dd if=/dev/zero of=/swap bs=1M count=40000
sudo chmod 600 /swap
sudo mkswap /swap
sudo swapon /swap
swapon -s
free -h
```

Si vous ne pouvez pas créer les fichiers automatiquement, voici des commandes à
exécuter manuellement (Debian) :

```sh
cd /var/www/html/Omeka

mkdir -p files/rameau/source files/rameau/data files/temp
wget 'ftp://databnf:databnf@pef.bnf.fr/DATA/databnf_rameau_xml.tar.gz' -O 'files/temp/databnf_rameau_xml.tar.gz'
tar -xf ./files/temp/databnf_rameau_xml.tar.gz -C ./files/rameau/source/

echo '<?xml version="1.0" encoding="UTF-8"?>' > files/rameau/data/fichiers.xml
echo '<list>' >> files/rameau/data/fichiers.xml
for f in $( ls files/rameau/source | sort ); do echo -e "    <entry name=\"$f\"/>" >> files/rameau/data/fichiers.xml ; done
echo '</list>' >> files/rameau/data/fichiers.xml

CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/rameau_skos.xsl' -s:'files/rameau/data/fichiers.xml' -o:'files/rameau/data/skos.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/thesaurus.xsl' -s:'files/rameau/data/skos.xml' -o:'files/rameau/data/thesaurus.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/valuesuggest.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/valuesuggest_rameau.xml'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.uri.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.uri.txt'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.label.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.label.txt'
CLASSPATH=/usr/share/java/Saxon-HE.jar java -Xms2g -Xmx4g net.sf.saxon.Transform -ext:on -versionmsg:off -xsl:'modules/Rameau/data/xsl/solr_synonyms.label_uri.xsl' -s:'files/rameau/data/thesaurus.xml' -o:'files/rameau/data/synonyms_fr.label_uri.txt'

chown -R www-data:www-data files/rameau
```

L’ensemble du traitement prend quelques minutes sur un serveur standard.

Actuellement, les fichiers originaux de Rameau sont mis à jour une ou deux fois
par an.


TODO
----

- [ ] Utiliser XmlReader pour la création des fichiers de skos pour éviter Saxon.
- [ ] Créer une sortie pour la [gestion automatique des synonymes](https://lucene.apache.org/solr/guide/8_7/filter-descriptions.html#managed-synonym-graph-filter) dans Solr.
- [ ] Ne mettre dans le fichier des synonymes que les synonymes présents dans la base (et la mettre à jour via la gestion automatique des synonymes).
- [ ] Créer l’arborescence Rameau ou intégrer un autre outil de thésaurus.


Attention
---------

Utilisez-le à vos propres risques.

Il est toujours recommandé de sauvegarder vos fichiers et vos bases de données
et de vérifier vos archives régulièrement afin de pouvoir les reconstituer si
nécessaire.


Dépannage
---------

Voir les problèmes en ligne sur la page des [questions du module] du GitLab.


Licence
-------

Ce module est publié sous la licence [CeCILL v2.1], compatible avec [GNU/GPL] et
approuvée par la [FSF] et l’[OSI].

Ce logiciel est régi par la licence CeCILL de droit français et respecte les
règles de distribution des logiciels libres. Vous pouvez utiliser, modifier
et/ou redistribuer le logiciel selon les termes de la licence CeCILL telle que
diffusée par le CEA, le CNRS et l’INRIA à l’URL suivante "http://www.cecill.info".

En contrepartie de l’accès au code source et des droits de copie, de
modification et de redistribution accordée par la licence, les utilisateurs ne
bénéficient que d’une garantie limitée et l’auteur du logiciel, le détenteur des
droits patrimoniaux, et les concédants successifs n’ont qu’une responsabilité
limitée.

À cet égard, l’attention de l’utilisateur est attirée sur les risques liés au
chargement, à l’utilisation, à la modification et/ou au développement ou à la
reproduction du logiciel par l’utilisateur compte tenu de son statut spécifique
de logiciel libre, qui peut signifier qu’il est compliqué à manipuler, et qui
signifie donc aussi qu’il est réservé aux développeurs et aux professionnels
expérimentés ayant des connaissances informatiques approfondies. Les
utilisateurs sont donc encouragés à charger et à tester l’adéquation du logiciel
à leurs besoins dans des conditions permettant d’assurer la sécurité de leurs
systèmes et/ou de leurs données et, plus généralement, à l’utiliser et à
l’exploiter dans les mêmes conditions en matière de sécurité.

Le fait que vous lisez actuellement ce document signifie que vous avez pris
connaissance de la licence CeCILL et que vous en acceptez les termes.

Voir [ci-dessus] pour la licence du vocabulaire.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2024 (voir [Daniel-KM] sur GitLab)

Ces fonctionnalités sont destinées à la bibliothèque numérique [Manioc] de
l’Université des Antilles et Université de la Guyane, auparavant gérée avec
[Greenstone].


[Rameau]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau
[English readme]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau/blob/master/README.md
[Omeka S]: https://omeka.org/s
[RAMEAU]: http://rameau.bnf.fr
[Common]: https://gitlab.com/Daniel-KM/Omeka-S-module-Common
[Value Suggest]: https://github.com/omeka-s-modules/ValueSuggest
[Search Solr]: https://gitlab.com/Daniel-KM/Omeka-S-module-SearchSolr
[Licence ouverte]: https://www.etalab.gouv.fr/licence-ouverte-open-licence
[Etalab]: https://www.etalab.gouv.fr
[ici]: https://www.bnf.fr/sites/default/files/2018-11/metadonnees_bnf_cond_utilisation.pdf
[et ici]: https://www.bnf.fr/fr/conditions-de-reutilisations-des-donnees-de-la-bnf
[ici aussi]: https://data.bnf.fr/licence
[installation d’un module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[indexation et en requête]: https://lucene.apache.org/solr/guide/8_6/filter-descriptions.html#synonym-graph-filter
[questions du module]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[MIT]: https://github.com/sandywalker/webui-popover/blob/master/LICENSE.txt
[ci-dessus]: https://gitlab.com/Daniel-KM/Omeka-S-module-Rameau/blob/master/LISEZMOI.md#remarques-sur-la-licence-du-vocabulaire
[Manioc]: http://www.manioc.org
[Greenstone]: http://www.greenstone.org
[GitLab]: https://gitlab.com/Daniel-KM
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
