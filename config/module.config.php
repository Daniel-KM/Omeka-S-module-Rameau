<?php declare(strict_types=1);

namespace Rameau;

return [
    'data_types' => [
        'factories' => [
            'valuesuggest:bnf:rameau' => Service\RameauDataTypeFactory::class,
        ],
    ],
    'form_elements' => [
        'invokables' => [
            Form\ConfigForm::class => Form\ConfigForm::class,
        ],
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => dirname(__DIR__) . '/language',
                'pattern' => '%s.mo',
                'text_domain' => null,
            ],
        ],
    ],
    'rameau' => [
        'config' => [
            'rameau_xslt_processor' => '',
        ],
    ],
];
