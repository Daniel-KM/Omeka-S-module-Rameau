<?php declare(strict_types=1);

namespace Rameau;

use Laminas\Mvc\Controller\AbstractController;
use Laminas\View\Renderer\PhpRenderer;
use Omeka\Module\AbstractModule;
use Omeka\Stdlib\Message;
use Rameau\Job\RameauExtract;

class Module extends AbstractModule
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getConfigForm(PhpRenderer $renderer)
    {
        $services = $this->getServiceLocator();
        $settings = $services->get('Omeka\Settings');
        $form = $services->get('FormElementManager')->get(\Rameau\Form\ConfigForm::class);

        $data = [
            'rameau_xslt_processor' => $settings->get('rameau_xslt_processor'),
        ];
        $form->setData($data);

        $html = '<p>'
            . '<ul>'
            . '<li><a href="https://rameau.bnf.fr/" target="_blank" rel="noopener">Site Rameau</a></li>'
            . '<li><a href="https://rameau.bnf.fr/sites/default/files/docs_reference/pdf/guide_rameau_2017.pdf" target="_blank" rel="noopener">Guide Rameau (pdf)</a></li>'
            . '<li><a href="http://api.bnf.fr/dumps-de-databnffr" target="_blank" rel="noopener">Fichiers sources</a></li>'
            . '</ul>'
            . '</p>';

        return $html
            . $renderer->formCollection($form);
    }

    public function handleConfigForm(AbstractController $controller)
    {
        $services = $this->getServiceLocator();
        $form = $services->get('FormElementManager')->get(\Rameau\Form\ConfigForm::class);
        $params = $controller->getRequest()->getPost();
        $form->setData($params);
        if (!$form->isValid()) {
            $controller->messenger()->addErrors($form->getMessages());
            return false;
        }

        $params = $form->getData();

        $settings = $services->get('Omeka\Settings');
        $settings->set('rameau_xslt_processor', $params['rameau_xslt_processor']);

        if (empty($params['process']) || $params['process'] !== $controller->translate('Process')) {
            $message = 'No job launched.'; // @translate
            $controller->messenger()->addWarning($message);
            return true;
        }

        /** @var \Omeka\Module\Manager $moduleManager */
        $moduleManager = $services->get('Omeka\ModuleManager');
        $hasLog = $moduleManager->getModule('Log');
        $hasLog = $hasLog && $hasLog->getState() === \Omeka\Module\Manager::STATE_ACTIVE;
        $urlPlugin = $controller->url();

        unset($params['csrf']);
        unset($params['rameau_xslt_processor']);
        $params['process'] = 'rameau';
        $params['remove_downloaded_file'] = (bool) $params['remove_downloaded_file'];
        $params['remove_existing_files'] = (bool) $params['remove_existing_files'];

        $dispatcher = $services->get(\Omeka\Job\Dispatcher::class);
        $job = $dispatcher->dispatch(RameauExtract::class, $params);
        $message = new Message(
            'Downloading Rameau and extracting data in background (job %1$s#%2$d%4$s, %3$slogs%4$s).', // @translate
            sprintf('<a href="%s">', htmlspecialchars($urlPlugin->fromRoute('admin/id', ['controller' => 'job', 'id' => $job->getId()]))),
            $job->getId(),
            sprintf('<a href="%s">', $hasLog ? $urlPlugin->fromRoute('admin/default', ['controller' => 'log'], ['query' => ['job_id' => $job->getId()]]) :  $urlPlugin->fromRoute('admin/id', ['controller' => 'job', 'action' => 'log', 'id' => $job->getId()])),
            '</a>'
        );
        $message->setEscapeHtml(false);
        $controller->messenger()->addSuccess($message);
        return true;
    }
}
